<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>無料体験申し込み | スパトレ</title>
<link rel="stylesheet" href="style.css">
<script type="text/javascript" src="contact.js"></script>
</head>
<body>
<div><h1>スパトレ無料体験お申し込み</h1></div>
<div><h2>申し込みフォーム</h2></div>
<div>
	<form action="confirm.php" method="post" name="form" onsubmit="return validate()">
		<p>お申し込み内容をご入力の上、「確認画面へ」ボタンをクリックしてください。</p>
		<div>
                	<div>
				<label>お名前<span>必須</span></label>
				<input type="text" name="name" placeholder="例）山田太郎" value="">
			</div>
			<div>
				<label>ふりがな<span>必須</span></label>
				<input type="text" name="furigana" placeholder="例）やまだたろう" value="">
			</div>
			<div>
				<label>メールアドレス<span>必須</span></label>
				<input type="text" name="email" placeholder="例）guest@example.com" value="">
			</div>
			<div>
				<label>電話番号<span>必須</span></label>
				<input type="text" name="tel" placeholder="例）0000000000" value="">
			</div>
			<div>
				<label>ご質問</label>
				<textarea name="content" rows="5" placeholder="ご質問を入力"></textarea>
			</div>
		</div>
		<button type="submit">確認画面へ</button>
	</form>
</div>
</body>
</html>
