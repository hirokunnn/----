<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>お問い合わせ | スパトレ</title>
<link rel="stylesheet" href="style.css">
<script type="text/javascript" src="contact.js"></script>
</head>
<body>
<div><h1>お問い合わせ</h1></div>
<div><h2>お問い合わせフォーム</h2></div>
<div>
	<form action="confirm2.php" method="post" name="form" onsubmit="return validate()">
		<p>お問い合わせ内容をご入力の上、「確認画面へ」ボタンをクリックしてください。</p>
		<div>
                	<div>
				<label>お名前<span>必須</span></label>
				<input type="text" name="name" placeholder="例）山田太郎" value="">
			</div>
			<div>
				<label>ふりがな<span>必須</span></label>
				<input type="text" name="furigana" placeholder="例）やまだたろう" value="">
			</div>
			<div>
				<label>メールアドレス<span>必須</span></label>
				<input type="text" name="email" placeholder="例）guest@example.com" value="">
			</div>
			<div>
				<label>電話番号<span>必須</span></label>
				<input type="text" name="tel" placeholder="例）0000000000" value="">
			</div>
			<div>
				<label>お問い合わせ内容</label>
				<textarea name="content" rows="5" placeholder="お問い合わせ内容をご入力ください"></textarea>
			</div>
		</div>
		<button type="submit">確認画面へ</button>
	</form>
</div>
</body>
</html>
